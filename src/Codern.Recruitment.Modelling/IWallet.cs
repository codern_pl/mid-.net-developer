﻿namespace Codern.Recruitment.Modelling;

public interface IWallet
{
    decimal GetBalance();
    void MakeTransaction(decimal amount);
}