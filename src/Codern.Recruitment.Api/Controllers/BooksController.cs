using Codern.Recruitment.Core.Dtos;
using Codern.Recruitment.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Codern.Recruitment.Api.Controllers;

/// <inheritdoc />
[ApiController]
[Route("books")]
public class BooksController : ControllerBase
{
    private readonly IBooksService _booksService;

    /// <inheritdoc />
    public BooksController(IBooksService booksService)
    {
        _booksService = booksService;
    }
    
    /// <summary>
    /// Get books page
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <param name="createdFrom"></param>
    /// <param name="createdTo"></param>
    /// <param name="pageNumber"></param>
    /// <param name="pageSize"></param>
    /// <returns></returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [HttpGet]
    public async Task<IEnumerable<BookDto>> Get(CancellationToken cancellationToken, DateTime? createdFrom = null, DateTime? createdTo = null, int pageNumber = 1, int pageSize = 10)
    {
        return await _booksService.GetPageAsync(createdFrom, createdTo, pageNumber, pageSize, cancellationToken);
    }
}