﻿using Codern.Recruitment.Core.Dtos;

namespace Codern.Recruitment.Core.Services;

public interface IBooksService
{
    Task<IEnumerable<BookDto>> GetPageAsync(DateTime? createdFrom, DateTime? createdTo, int pageNumber, int pageSize,
        CancellationToken cancellationToken);
}