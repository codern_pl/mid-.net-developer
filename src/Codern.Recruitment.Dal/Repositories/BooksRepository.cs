﻿using Codern.Recruitment.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Codern.Recruitment.Dal.Repositories;

internal class BooksRepository : IBooksRepository
{
    private readonly CodernDbContext _dbContext;

    public BooksRepository(CodernDbContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<IEnumerable<Book>> GetPageAsync(DateTime? createdFrom, DateTime? createdTo, int pageNumber, int pageSize, CancellationToken cancellationToken)
    {
        var query = _dbContext.Books.AsQueryable();

        if (createdFrom.HasValue)
        {
            query = query.Where(x => x.CreatedAtUtc >= createdFrom);
        }
        
        if (createdTo.HasValue)
        {
            query = query.Where(x => x.CreatedAtUtc <= createdTo);
        }
        
        if (pageNumber == 1)
        {
            return await query.Skip(0).Take(pageSize).ToListAsync(cancellationToken);
        }

        return await query
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync(cancellationToken);
    }
}