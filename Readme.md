﻿# Welcome to Codern .NET Recruitment

* you can use internet and all needed tools but it has to be visible for the recruiter (we want to recreate "real work environment" as closely as possible)

* you can use all c# features, create new files, install nuget packages etc.

## Part 1 - Web Api Test
#### Code structure
Web api part contains 3 projects (all written in .NET 6). It is commonly known "layered architecture". Project uses Entity Framework Core with In-Memory Database
provider. Database Seed is performed on application startup.

| Project                  | Description |
|--------------------------| ----------- |
| Codern.Recruitment.Api   | REST api project |
| Codern.Recruitment.Core  | Business Logic project |
| Codern.Recruitment.Dal   | Data Access Layer project |

#### Your tasks
1.Create PUT endpoint to update book. Validation should be:

* title cannot be empty

* max title lengths  250 

* ISBN has to be unique

## Part 2 - Object Oriented Programming Test
You need to implement basic wallet model **with unit tests**. Starting point should be `IWallet` interface. Please create `Wallet` class and implement the interface.

* your code has to cover all test cases in unit tests project (`Codern.Recruitment.Modelling.Tests`)

* you can add test cases it you find some missing ones

* you can change `IWallet` contract if needed